﻿using System;

namespace _21
{
    class Program
    {
        static void Main(string[] args)
        {
           //Start the program with Clear();
           Console.Clear();
           int i=1;
           while ( i <6 )
           {
               Console.WriteLine(i);
               i++;
           }
           
           Console.ReadKey();
           for (int a = 1; a < 6; a++ )
           {
               Console.WriteLine(a);

           }
           Console.ReadKey();
           
           for (int b = 1; b <=20; b++)
           {
               while (b % 2 == 0)
               {
                   Console.WriteLine(b); 
                   b++;
               }
           }
           Console.ReadKey();

           var counter =6;
           var n= 7;    // n is index
           do
           {
               Console.WriteLine($"The value of counter is{counter}");
               Console.WriteLine($"The value of index is{n}");
               if(n>counter)
               {
                   Console.WriteLine("Index is bigger than counter");
               }
               else
               {

               }
               n++;
               Console.WriteLine(" ");
           }while(n<counter);

           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey(); 
        }
    }
}

